﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class EventList
    {
        public List<EventViewModel> Events { get; set; }
    }
}