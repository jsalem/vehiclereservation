﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class CarList
    {
        public List<CarViewModel> Cars { get; set; }
    }
}