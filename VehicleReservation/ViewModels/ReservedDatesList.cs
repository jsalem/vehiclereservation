﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class ReservedDatesList
    {
        public List<ReservedDates> Dates { get; set; }

    }
}