﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Foolproof;
namespace VehicleReservation.ViewModels
{
    //public class CheckDateAttribute : ModelAwareValidationAttribute
    //{
    //    static CheckDateAttribute() { Register.Attribute(typeof(CheckDateAttribute)); }
        
    //    public override bool IsValid(object value, object container)
    //    {
    //        VehicleReservationEntities db = new VehicleReservationEntities();


    //        if (value != null)
    //        {
    //            string sdate = value.ToString();
    //            ReservationViewModel RVM = new ReservationViewModel();

    //            var checkDate = (from a in db.DriverVehicles
    //                             where a.VehicleID == RVM.VehicleID && a.IsComplete == false
    //                             select new
    //                             {
    //                                 a.StartDateTime,
    //                                 a.EndDateTime
    //                             }).SingleOrDefault();

    //            if (sdate >= checkDate.StartDateTime.ToString() && RVM.DateTimeStart <= checkDate.EndDateTime)

    //        }

    //}
    public class ReservationViewModel
    {   
        public int Id { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [RegularExpression("^[a-zA-Z\\.\\- +]*$", ErrorMessage = "Only letters are accepted")]
        public string BorrowerLastName { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [RegularExpression("^[a-zA-Z\\.\\- +]*$", ErrorMessage = "Only letters are accepted")]
        public string BorrowerFirstName { get; set; }

        [Required(ErrorMessage = "Vehicle is required")]
        public int? VehicleID { get; set; }

        [Required(ErrorMessage = "Driver is required")]
        public int? DriverID { get; set; }
        public string VehicleMake { get; set; }
        //public  string DriverName { get; set; }
        public string VehicleType { get; set; }

        [Required(ErrorMessage = "Odometer Start is required")]
        [RegularExpression("^[0-9,.]*$", ErrorMessage = "Only numbers are accepted")]
        public string OdometerStart { get; set; }

        [Required(ErrorMessage = "Odometer End is required")]
        [RegularExpression("^[0-9,.]*$", ErrorMessage = "Only numbers are accepted")]
        [GreaterThan("OdometerStart", ErrorMessage = "Must be greater than Odometer Start")]
        public string OdometerEnd { get; set; }

        //[Required(ErrorMessage = "Liters is required")]
        [RegularExpression("^[0-9,.]*$", ErrorMessage = "Only numbers are accepted")]
        public string Liters { get; set; }

        //[Required(ErrorMessage = "Amount Paid is required")]
        [RegularExpression("^[0-9,.]*$", ErrorMessage = "Only numbers are accepted")]
        public decimal? AmountPaid { get; set; }

        //[Required(ErrorMessage = "OR No. is required")]
        public string ORNumber { get; set; }

        [Required(ErrorMessage = "Meet-up Place is required")]
        public string Location { get; set; }

        [Required(ErrorMessage = "Reason is required")]
        public string Reason { get; set; }

        [Required(ErrorMessage = "Project ID is required")]
        public string ProjectID { get; set; }
        public DateTime? DateCreated { get; set; }

        [Required(ErrorMessage = "User ID is required")]
        public int? UserID { get; set; }

        [Required(ErrorMessage = "Number of Passenger is required")]
        public int? NumberofPassenger { get; set; }

        public int? VehicleTypeID { get; set; }

        [Required(ErrorMessage = "Date/Time Start is required")]
        //[CheckDate(ErrorMessage = "Vehicle can't be reserved on this date/time.")]
        public DateTime? DateTimeStart { get; set; }


        [Required(ErrorMessage = "Date/Time End is required")]
        [GreaterThan("DateTimeStart", ErrorMessage = "Must be greater than Date/Time Start")]
        public DateTime? DateTimeEnd { get; set; }

        public bool? IsActive { get; set; }

        [Required(ErrorMessage = "Location Parked is required")]
        public string LocationParked { get; set; }
        //
        public string BorrowerFullNameList
        {
            get { return BorrowerLastName + ", " + BorrowerFirstName; }
            set { }
        }
        public string BorrowerFullName { get; set; }

        public string VehicleMakeLicense
        {
            get { return VehicleMake + " - " + PlateNumber; }
            set { }
        }

        //public string VehicleMakeLicense { get; set; }
        //
        public string DriverLastName { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverName { get; set;}
        public string PlateNumber { get; set; }
        public bool IsReserved { get; set; }
        public bool? IsComplete { get; set; }

        [Required(ErrorMessage = "Actual Date/Time Start is required")]
        public DateTime? ActualDateTimeStart { get; set; }

        [Required(ErrorMessage = "Actual Date/Time End is required")]
        public DateTime? ActualDateTimeEnd { get; set; }

        public int? BusinessUnitID { get; set; }
        public string BusinessUnit { get; set; }
        public int? RequestedBy { get; set; }

        public List<Users> Users { get; set; }
        public string BorrowerName { get; set; }
        public int? ModifiedBy { get; set; }
        public string ReservedDate { get; set; }
        public int? CodingDay { get; set; }
    }
}