﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class VehicleListVM
    {
        public List<VehicleViewModel> Vehicles { get; set; }
        public List<DriverViewModel> Drivers { get; set; }


    }
}