﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class ReservedDates
    {
        public DateTime? DateTimeStart;
        public DateTime? DateTimeEnd;
        public int? CodingDay;
        public string PlateNumber;
        public string VehicleMake;
        public int? DriverID;
        public string Name;

    }
}