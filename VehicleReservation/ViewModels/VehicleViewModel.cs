﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace VehicleReservation.ViewModels
{
    public class VehicleViewModel
    {
        public int VehicleID { get; set; }



        [Required(ErrorMessage = "Vehicle Type is required")]
        public string VehicleType { get; set; }

        [Required(ErrorMessage = "Vehicle Make is required")]
        public string VehicleMake { get; set; }

        [Required(ErrorMessage = "Plate Number is required")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string PlateNumber { get; set; }

        [Required(ErrorMessage = "Coding Day is required")]
        public int? CodingDay { get; set; }

        [Required(ErrorMessage = "Location is required")]
        public string Location { get; set; }

        [Required(ErrorMessage = "Driver  is required")]
        public int? DriverID { get; set; }

        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> DateRegistered { get; set; }
        public Nullable<bool> IsReserved { get; set; }

        public int Id { get; set; }
        public DateTime DateCreated { get; set; }


        public string DriverLastName { get; set; }
        public string DriverFirstName { get; set; }
        public string FullNameList
        {
            get { return DriverLastName + ", " + DriverFirstName; }
            set { }
        }
        public string FullName { get; set; }

        public string CodingDayText { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }

        public int? ModifiedBy { get; set; }
        public string Name { get; set; }

        public int VehicleTypeID { get; set; }
    }
}