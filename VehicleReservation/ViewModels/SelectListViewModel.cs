﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class SelectListViewModel
    {
        public String Text { get; set; }
        public String Value { get; set; }
    }
}