﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class CarViewModel
    {
        public int? CodingDay;
        public DateTime? DateTimeStart;
        public DateTime? DateTimeEnd;
    }
}