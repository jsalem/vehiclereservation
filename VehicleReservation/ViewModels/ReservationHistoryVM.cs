﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class ReservationHistoryVM
    {
        public List<ReservationViewModel> Reservations { get; set; }
    }
}