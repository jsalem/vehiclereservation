﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class DriverListVM
    {
        public List<DriverViewModel> Drivers { get; set; }

    }
}