﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class EventViewModel
    {
        public string title;
        public DateTime? start;
        public DateTime? end;
        public string description;
        public string allDay;
        public string description2;
        public string description3;
        public bool? isActive;
        public bool? isComplete;
    }
}