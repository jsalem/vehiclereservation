﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace VehicleReservation.ViewModels
{
    public class DriverViewModel
    {
        public int DriverID { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [RegularExpression("^[a-zA-Z\\.\\- +]*$", ErrorMessage = "Only Letters are accepted")]
        public string DriverLastName { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [RegularExpression("^[a-zA-Z\\.\\- +]*$", ErrorMessage = "Only Letters are accepted")]
        public string DriverFirstName { get; set; }

        [Required(ErrorMessage = "Mobile Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers are accepted")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Must be 11 characters long")]
        public string MobileNumber { get; set; }

        [Required(ErrorMessage = "Driver's License is required")]
        public string DriverLicense { get; set; }

        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "License Expiry is required")]
        //[DisplayFormat(DataFormatString = "{0:YYYY-MM-DD)")]
        public DateTime? LicenseExpiry { get; set; }

        [Required(ErrorMessage = "Business Unit ID is required")]
        public string BusinessUnit { get; set; }
        public bool IsActive { get; set; }

        public int? BusinessUnitID { get; set; }

        public System.DateTime DateRegistered { get; set; }

        public string FullName
        {
            get { return DriverLastName + ", " + DriverFirstName; }
        }

        public List<Users> Users { get; set; }

        public int? ModifiedBy { get; set; }
        public int? UserID { get; set; }
        public string Name { get; set; }
    }
}