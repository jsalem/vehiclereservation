﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleReservation.ViewModels
{
    public class Users
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string ImmediateManagerEmployeeID { get; set; }
        public string ImmediateManager { get; set; }

        public int? AttendanceTypeID { get; set; }
        public string AttendanceTypeName { get; set; }


        public int BusinessUnitID { get; set; }
        public string BusinessUnit { get; set; }
        public string EmploymentStatus { get; set; }
        public bool IsActive { get; set; } = false;

    }
}