﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VehicleReservation.ViewModels;
using VehicleReservation.Repository;

namespace VehicleReservation.Controllers
{
    public class VehicleController : Controller
    {
        VehicleReservationEntities db = new VehicleReservationEntities();

        //LIST
        public ActionResult List()
        {

            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            var repo = new ManagementRepository();
            //var model = repo.ListVehicle();

            VehicleListVM VLVM = new VehicleListVM();
            VLVM.Vehicles = new List<VehicleViewModel>();


            VLVM.Vehicles = (from data in db.Vehicles
                             join b in db.Drivers on data.DriverID equals b.DriverID into list1
                             from l1 in list1.DefaultIfEmpty()
                                 //join c in db.DriverVehicles on data.VehicleID equals c.VehicleID into list2
                                 //from l2 in list2.DefaultIfEmpty()
                             select new VehicleViewModel()
                             {
                                 VehicleID = data.VehicleID,
                                 VehicleType = data.VehicleType,
                                 VehicleMake = data.VehicleMake,
                                 PlateNumber = data.PlateNumber,
                                 CodingDay = data.CodingDay,
                                 Location = data.Location,
                                 DriverID = data.DriverID,
                                 DateRegistered = data.DateRegistered,
                                 //FullName = b.DriverLastName + ", " + b.DriverFirstName,
                                 Name = l1.Name,
                                 IsReserved = data.IsReserved,
                                 IsActive = data.IsActive,
                                 VehicleTypeID = data.VehicleTypeID
                                 //StartDateTime = l2.StartDateTime,
                                 //EndDateTime = l2.EndDateTime
                             }).ToList();

    


            return View(VLVM);

            //var VM = new VehicleViewModel();
            //var existingVehicle = (from a in db.Vehicles
            //                       select new
            //                       {
            //                           a.VehicleID,
            //                           a.VehicleMake,
            //                           a.VehicleType,
            //                           a.PlateNumber,
            //                           a.CodingDay,
            //                           a.Location,
            //                           a.DriverID,
            //                           a.IsActive,
            //                           a.IsReserved
            //                       }).FirstOrDefault();
            //var LinkedDriver = (from a in db.Vehicles
            //                    join b in db.Drivers
            //                    on a.DriverID equals b.DriverID
            //                    select new
            //                    {
            //                        a.DriverID,
            //                        b.DriverLastName,
            //                        b.DriverFirstName
            //                    }).FirstOrDefault();

            //VM.VehicleID = existingVehicle.VehicleID;
            //VM.VehicleMake = existingVehicle.VehicleMake;
            //VM.VehicleType = existingVehicle.VehicleType;
            //VM.PlateNumber = existingVehicle.PlateNumber;
            //VM.CodingDay = existingVehicle.CodingDay;
            //VM.Location = existingVehicle.Location;
            //VM.DriverID = existingVehicle.DriverID;
            //VM.IsActive = existingVehicle.IsActive;
            //VM.IsReserved = existingVehicle.IsReserved;

            //if (VM.CodingDay == "1")
            //{
            //    VM.CodingDayText = "Monday";
            //}

            //if (VM.CodingDay == "2")
            //{
            //    VM.CodingDayText = "Tuesday";
            //}

            //if (VM.CodingDay == "3")
            //{
            //    VM.CodingDayText = "Wednesday";
            //}

            //if (VM.CodingDay == "4")
            //{
            //    VM.CodingDayText = "Thursday";
            //}

            //if (VM.CodingDay == "5")
            //{
            //    VM.CodingDayText = "Friday";
            //}

            //if (VM.DriverID == 0)
            //{
            //    VM.FullName = "N/A";

            //}

            //if (VM.DriverID > 0)
            //{

            //    VM.FullName = LinkedDriver.DriverLastName + ", " + LinkedDriver.DriverFirstName;

            //}

            //return View(VM);


        }

        //DETAILS
        public ActionResult Details(int? id)
        {
            //var repo = new ManagementRepository();
            //var model = repo.GetVehicleDetails(id);
            //var VVM = new VehicleViewModel();

            //VVM.VehicleID = model.VehicleID;
            //VVM.VehicleType = model.VehicleType;
            //VVM.VehicleMake = model.VehicleMake;
            //VVM.PlateNumber = model.PlateNumber;
            //VVM.CodingDay = model.CodingDay;
            //VVM.DriverID = model.DriverID;
            //VVM.Location = model.Location;

            //var repo = new ManagementRepository();
            //var DRV = new Driver();
            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            Vehicle vehicle = db.Vehicles.Find(id);
            var VVM = new VehicleViewModel();
            var existingVehicle = (from a in db.Vehicles
                                   where a.VehicleID == id
                                   select new
                                   {
                                       a.VehicleID,
                                       a.VehicleMake,
                                       a.VehicleType,
                                       a.PlateNumber,
                                       a.CodingDay,
                                       a.Location,
                                       a.DriverID,
                                       a.VehicleTypeID,
                                       //b.DriverFirstName,
                                       //b.DriverLastName
                                   }).FirstOrDefault();

            var existingVehicleOffice = (from a in db.Vehicles
                                         join b in db.Drivers
                                         on a.DriverID equals b.DriverID
                                         where a.VehicleID == id
                                         select new
                                         {
                                             a.DriverID,
                                             b.Name
                                         }).FirstOrDefault();

            VVM.VehicleID = existingVehicle.VehicleID;
            VVM.VehicleMake = existingVehicle.VehicleMake;
            VVM.VehicleType = existingVehicle.VehicleType;
            VVM.PlateNumber = existingVehicle.PlateNumber;
            VVM.CodingDay = existingVehicle.CodingDay;
            VVM.Location = existingVehicle.Location;
            VVM.DriverID = existingVehicle.DriverID;
            VVM.VehicleTypeID = existingVehicle.VehicleTypeID;

            if (VVM.DriverID == 0)
            {
                VVM.FullName = "N/A";

            }

            if (VVM.DriverID > 0)
            {

                //VVM.FullName = existingVehicleOffice.DriverLastName + ", " + existingVehicleOffice.DriverFirstName;
                VVM.Name = existingVehicleOffice.Name;
            }

            if (VVM.CodingDay == 1)
            {
                VVM.CodingDayText = "Monday";
            }

            if (VVM.CodingDay == 2)
            {
                VVM.CodingDayText = "Tuesday";
            }

            if (VVM.CodingDay == 3)
            {
                VVM.CodingDayText = "Wednesday";
            }

            if (VVM.CodingDay == 4)
            {
                VVM.CodingDayText = "Thursday";
            }

            if (VVM.CodingDay == 5)
            {
                VVM.CodingDayText = "Friday";
            }



            //var existingVehicle = (from a in db.Vehicles
            //                       //join b in db.Drivers on a.DriverID equals b.DriverID
            //                       where a.VehicleID == id
            //                       select new VehicleViewModel()
            //                       {
            //                           VehicleID = a.VehicleID,
            //                           VehicleType = a.VehicleType,
            //                           VehicleMake = a.VehicleMake,
            //                           PlateNumber = a.PlateNumber,
            //                           CodingDay = a.CodingDay,
            //                           //FullName = b.DriverLastName + ", " + b.DriverFirstName,
            //                           Location = a.Location
            //                       }).FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(VVM);
        }


        //ADD
        /*      public ActionResult Add()
              {
                  ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "DriverLastName");

                  return View();
              }

              [HttpPost]
              public ActionResult Add(VehicleViewModel VVM)
              {

                  var repo = new ManagementRepository();
                  var vehicle = new Vehicle();
                  var driver = new Driver();

                  ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "DriverLastName", vehicle.DriverID);

                  vehicle.VehicleID = VVM.VehicleID;
                  vehicle.VehicleType = VVM.VehicleType;
                  vehicle.VehicleMake = VVM.VehicleMake;
                  vehicle.PlateNumber = VVM.PlateNumber;
                  vehicle.CodingDay = VVM.CodingDay;
                  vehicle.Location = VVM.Location;
                  vehicle.DriverID = VVM.DriverID;
                  vehicle.IsActive = VVM.IsActive;
                  vehicle.DateRegistered = DateTime.Now;

                  repo.AddVehicle(vehicle);
                  return RedirectToAction("List");
              }
              */

        public ActionResult AddOfficeType()
        {
            ViewBag.DriverID = db.Drivers.Select(a => new SelectListItem
            {
                Value = a.DriverID.ToString(),
                Text = a.DriverLastName + ", " + a.DriverFirstName
            });
            

            return View();
        }

        [HttpPost]
        public ActionResult AddOfficeType(VehicleViewModel VVM)
        {
            var repo = new ManagementRepository();
            var vehicle = new Vehicle
            {
                VehicleID = VVM.VehicleID,
                VehicleType = "Office",
                VehicleMake = VVM.VehicleMake,
                PlateNumber = VVM.PlateNumber,
                CodingDay = VVM.CodingDay,
                Location = VVM.Location,
                DriverID = VVM.DriverID,
                IsActive = false,
                IsReserved = false,
                DateRegistered = DateTime.Now



            };

            var newDriverVehicle = new DriverVehicle();


            using (var db = new VehicleReservationEntities())
            {
                repo.AddVehicle(vehicle);

                newDriverVehicle.Id = VVM.Id;
                newDriverVehicle.DriverID = VVM.DriverID;
                newDriverVehicle.VehicleID = vehicle.VehicleID;
                newDriverVehicle.DateCreated = DateTime.Now;
                newDriverVehicle.VehicleMake = VVM.VehicleMake;
                repo.AddDriverVehicle(newDriverVehicle);

            }

            return RedirectToAction("List");
        }

        public ActionResult AddVendorType()
        {

            return View();
        }

        [HttpPost]
        public ActionResult AddVendorType(VehicleViewModel VVM)
        {
            var repo = new ManagementRepository();
            var vehicle = new Vehicle();



            vehicle.VehicleID = VVM.VehicleID;
            vehicle.VehicleType = "Vendor";
            vehicle.VehicleMake = VVM.VehicleMake;
            vehicle.PlateNumber = VVM.PlateNumber;
            vehicle.CodingDay = VVM.CodingDay;
            vehicle.Location = VVM.Location;
            vehicle.IsActive = false;
            vehicle.IsReserved = false;
            vehicle.DateRegistered = DateTime.Now;
            vehicle.DriverID = 0;          

            repo.AddVehicle(vehicle);

            return RedirectToAction("List");



        }
        //ADD
        public ActionResult Add()
        {
            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }
            VehicleViewModel VVM = new VehicleViewModel();
            int? rb = Convert.ToInt32(Request.Cookies["UserID"].Value.ToString());


            if (db.Drivers == null)
            {
                ViewBag.Message = "There are no drivers registered.";
                return RedirectToAction("List", "Vehicle");
            }

            ViewBag.DriverID = db.Drivers.Select(a => new SelectListItem
            {
                Value = a.DriverID.ToString(),
                Text = a.Name
            });

            VVM.ModifiedBy = rb;


            return View(VVM);
        }

        [HttpPost]
        public ActionResult Add(VehicleViewModel VVM)
        {
            var repo = new ManagementRepository();
            var vehicle = new Vehicle
            {
                VehicleID = VVM.VehicleID,
                VehicleType = VVM.VehicleType,
                VehicleMake = VVM.VehicleMake,
                PlateNumber = VVM.PlateNumber,
                CodingDay = VVM.CodingDay,
                Location = VVM.Location,
                DriverID = VVM.DriverID,
                IsActive = false,
                IsReserved = false,
                DateRegistered = DateTime.Now,
                ModifiedBy = VVM.ModifiedBy
                
            };

            if (vehicle.DriverID == null)
            {
                vehicle.DriverID = 0;
            }

            repo.AddVehicle(vehicle);

            //var newDriverVehicle = new DriverVehicle();


            //using (var db = new VehicleReservationEntities())
            //{
            //    

            //    newDriverVehicle.Id = VVM.Id;
            //    newDriverVehicle.DriverID = VVM.DriverID;
            //    newDriverVehicle.VehicleID = vehicle.VehicleID;
            //    newDriverVehicle.DateCreated = DateTime.Now;
            //    newDriverVehicle.VehicleMake = VVM.VehicleMake;
            //    repo.AddDriverVehicle(newDriverVehicle);

            //}

            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult add_vehicle()
        {
            var repo = new ManagementRepository();
            var vehicle = new Vehicle();

            vehicle.VehicleMake = Request.Form["vehicle_make"].Trim();
            vehicle.PlateNumber = Request.Form["plate_number"].Trim();
            vehicle.CodingDay =  int.Parse(Request.Form["coding_day"]);
            vehicle.Location = Request.Form["location"].Trim();
            vehicle.VehicleTypeID = int.Parse(Request.Form["vehicle_type_id"]);
            vehicle.IsActive = false;
            vehicle.IsReserved = false;
            vehicle.DateRegistered = DateTime.Now;

            repo.AddVehicle(vehicle);

            return Json("", JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult edit_vehicle(int id)
        {
            var repo = new ManagementRepository();
            var vehicle = new Vehicle();

            vehicle.VehicleMake = Request.Form["vehicle_make"].Trim();
            vehicle.PlateNumber = Request.Form["plate_number"].Trim();
            vehicle.CodingDay = int.Parse(Request.Form["coding_day"]);
            vehicle.Location = Request.Form["location"].Trim();
            vehicle.VehicleTypeID = int.Parse(Request.Form["vehicle_type_id"]);
            vehicle.IsActive = false;
            vehicle.IsReserved = false;
            vehicle.VehicleID = id;

            repo.UpdateVehicle(vehicle);

            return Json("", JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult vehicle_details(int id)
        {
            var vehicle = (from v in db.Vehicles
                           where v.VehicleID == id
                           select new
                           {
                               v.VehicleID,
                               v.VehicleMake,
                               v.PlateNumber,
                               v.CodingDay,
                               v.Location,
                               v.VehicleTypeID
                           }).SingleOrDefault();
            return Json(vehicle, JsonRequestBehavior.AllowGet);
        }

        //EDIT
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            var repo = new ManagementRepository();
            var VVM = new VehicleViewModel();
            int? rb = Convert.ToInt32(Request.Cookies["UserID"].Value.ToString());


            ViewBag.DriverID = db.Drivers.Select(a => new SelectListItem
            {
                Value = a.DriverID.ToString(),
                Text = a.Name
            });
            Vehicle editVehicle = db.Vehicles.Find(id);

            VVM.VehicleID = editVehicle.VehicleID;
            VVM.DriverID = editVehicle.DriverID;
            VVM.VehicleType = editVehicle.VehicleType;
            VVM.VehicleMake = editVehicle.VehicleMake;
            VVM.PlateNumber = editVehicle.PlateNumber;
            VVM.CodingDay = editVehicle.CodingDay;
            VVM.Location = editVehicle.Location;
            VVM.ModifiedBy = rb;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(VVM);
        }

        [HttpPost]
        public ActionResult Edit(VehicleViewModel VVM)
        {

            var repo = new ManagementRepository();
            var vehicle = new Vehicle();
            using (var db = new VehicleReservationEntities())
            {
                var existingVehicle = (from data in db.Vehicles
                                      where data.VehicleID == VVM.VehicleID
                                      select data).FirstOrDefault();

                existingVehicle.VehicleID = VVM.VehicleID;
                existingVehicle.DriverID = VVM.DriverID;
                existingVehicle.VehicleType = VVM.VehicleType;
                existingVehicle.VehicleMake = VVM.VehicleMake;
                existingVehicle.PlateNumber = VVM.PlateNumber;
                existingVehicle.CodingDay = VVM.CodingDay;
                existingVehicle.Location = VVM.Location;
                existingVehicle.ModifiedBy = VVM.ModifiedBy;

                repo.UpdateVehicle(existingVehicle);
            }




            return RedirectToAction("List");

        }
    }
}