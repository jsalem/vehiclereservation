﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleReservation.Repository;
using VehicleReservation.ViewModels;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace VehicleReservation.Controllers
{
    public class DriverController : Controller
    {
        private VehicleReservationEntities db = new VehicleReservationEntities();
        //LIST
        public ActionResult List(string searchString)
        {
            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            var repo = new ManagementRepository();
            var model = repo.ListDriver();

            DriverListVM DLVM = new DriverListVM();
            DLVM.Drivers = new List<DriverViewModel>();

            DLVM.Drivers = (from data in model
                            select new DriverViewModel()
                            {
                                DriverID = data.DriverID,
                                   DriverLicense = data.DriverLicense,
                                   Name = data.Name,
                                   LicenseExpiry = data.LicenseExpiry,
                                   BusinessUnit = data.BusinessUnit,
                                   MobileNumber = data.MobileNumber


                               }).ToList();

            return View(DLVM);
        }

        //DETAILS
        public ActionResult Details(int id)
        {

            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            var repo = new ManagementRepository();
            var model = repo.GetDriverDetails(id);
            var DVM = new DriverViewModel();

            DVM.DriverID = model.DriverID;
            DVM.Name = model.Name;
            DVM.DriverLicense = model.DriverLicense;
            DVM.LicenseExpiry = model.LicenseExpiry;
            DVM.MobileNumber = model.MobileNumber;
            DVM.BusinessUnit = model.BusinessUnit;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(DVM);
        }

        //ADD
        public async Task<ActionResult> Add()
        {

            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            DriverViewModel VM = new DriverViewModel();
            string token = Convert.ToString(Request.Cookies["token"].Value.ToString());


            DataService DS = new DataService();
            VM.Users = await DS.GetUsers(token);

            int? rb = Convert.ToInt32(Request.Cookies["UserID"].Value.ToString());
            VM.ModifiedBy = rb;


            return View(VM);
        }

        public ActionResult _Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult add_driver()
        {
            string msg = "";
            var repo = new ManagementRepository();
            var driver = new Driver();
            
            driver.Name = Request.Form["driver_name"];
            driver.MobileNumber = Request.Form["mobile"];
            driver.DriverLicense = Request.Form["license_no"];
            driver.LicenseExpiry = DateTime.Parse(Request.Form["expiry_date"]);

            repo.AddDriver(driver);
            
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult edit_driver(int id)
        {
            string msg = "";
            var repo = new ManagementRepository();
            var driver = new Driver();

            driver.Name = Request.Form["driver_name"];
            driver.MobileNumber = Request.Form["mobile"];
            driver.DriverLicense = Request.Form["license_no"];
            driver.LicenseExpiry = DateTime.Parse(Request.Form["expiry_date"]);
            driver.DriverID = id;

            repo.UpdateDriver(driver);

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult driver_view(int id)
        {
            var repo = new ManagementRepository();
            var model = repo.GetDriverDetails(id);
            var DVM = new DriverViewModel();

            DVM.DriverID = model.DriverID;
            DVM.Name = model.Name;
            DVM.DriverLicense = model.DriverLicense;
            DVM.LicenseExpiry = model.LicenseExpiry;
            DVM.MobileNumber = model.MobileNumber;
            DVM.BusinessUnit = model.BusinessUnit;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(DVM);
        }

        [HttpPost]
        public ActionResult driver_details(int driver_id)
        {
            var driver = (from d in db.Drivers
                          where d.DriverID == driver_id
                          select new {
                              d.Name,
                              d.MobileNumber,
                              d.LicenseExpiry,
                              d.DriverLicense,
                         }).SingleOrDefault();
            return Json(driver, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult Add(DriverViewModel DVM)
        //{
        //    var repo = new ManagementRepository();
        //    var driver = new Driver();


        //    driver.Name = DVM.Name;
        //    driver.DriverLicense = DVM.DriverLicense;
        //    driver.LicenseExpiry = DVM.LicenseExpiry;
        //    driver.MobileNumber = DVM.MobileNumber;
        //    driver.BusinessUnitID = DVM.BusinessUnitID;
        //    driver.BusinessUnit = DVM.BusinessUnit;
        //    driver.UserID = DVM.UserID;
        //    driver.DateRegistered = DateTime.Now;
        //    driver.IsActive = false;
        //    driver.ModifiedBy = DVM.ModifiedBy;


        //    repo.AddDriver(driver);

        //    return RedirectToAction("List");
        //}

        //EDIT
        [HttpGet]
        public async Task<ActionResult> Edit(int? id)
        {


            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            var repo = new ManagementRepository();
            var DVM = new DriverViewModel();
            string token = Convert.ToString(Request.Cookies["token"].Value.ToString());



            DataService DS = new DataService();
            DVM.Users = await DS.GetUsers(token);
            int? rb = Convert.ToInt32(Request.Cookies["UserID"].Value.ToString());



            Driver editDriver = db.Drivers.Find(id);
            //   repo.UpdateDriver(editDriver);
            DVM.DriverID = editDriver.DriverID;
            DVM.Name = editDriver.Name;
            DVM.DriverLicense = editDriver.DriverLicense;
            DVM.LicenseExpiry = editDriver.LicenseExpiry;
            DVM.MobileNumber = editDriver.MobileNumber;
            DVM.BusinessUnit = editDriver.BusinessUnit;
            DVM.BusinessUnitID = editDriver.BusinessUnitID;
            DVM.UserID = editDriver.UserID;
            DVM.ModifiedBy = rb;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(DVM);
        }

        [HttpPost]
        public ActionResult Edit(DriverViewModel DVM)
        {

            var repo = new ManagementRepository();
            var driver = new Driver();
            using (var db = new VehicleReservationEntities())
            { 
                var existingDriver = (from data in db.Drivers
                                      where data.DriverID == DVM.DriverID
                                      select data).FirstOrDefault();

                existingDriver.DriverID = DVM.DriverID;
                existingDriver.Name = DVM.Name;
                existingDriver.DriverLicense = DVM.DriverLicense;
                existingDriver.LicenseExpiry = DVM.LicenseExpiry;
                existingDriver.MobileNumber = DVM.MobileNumber;
                existingDriver.BusinessUnit = DVM.BusinessUnit;
                existingDriver.BusinessUnitID = DVM.BusinessUnitID;
                existingDriver.UserID = DVM.UserID;
                existingDriver.ModifiedBy = DVM.ModifiedBy;

                repo.UpdateDriver(existingDriver);
            }




            return RedirectToAction("List");
            
        }

        

        //public String Users(string start, string end)
        //{
        //    UserList UL = new UserList();
        //    UL.Users = new List<UsersViewModel>();

        //    UL.Users = (from data in db.Drivers
        //                select new UsersViewModel()
        //                {
        //                    DriverID = data.DriverID,
        //                    DriverLastName = data.DriverLastName,
        //                    DriverFirstName = data.DriverFirstName

        //                }).ToList();
        //    var json = JsonConvert.SerializeObject(UL.Users);
        //    return json;

        //}

        //static HttpClient client = new HttpClient();

        //static async Task RunAsync()
        //{
        //    client.BaseAddress = new Uri("https://www.portal.smsgt.com/onesapi/api/attendance/Getusers");
        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(
        //        new MediaTypeWithQualityHeaderValue("application/json"));
        //}

        //static async Task<UsersViewModel> GetUsers(string path)
        //{
        //    UsersViewModel UVM = null;
        //    HttpResponseMessage response = await client.GetAsync(path);

        //    if (response.IsSuccessStatusCode)
        //    {
        //        UVM = await response.Content.ReadAsAsync<UsersViewModel>();
        //    }

        //    return UVM;
        //}

        //public Action LogInAdmin()
        //{

        //}

        //public Action LogInUser()
        //{

        //}

    }
}