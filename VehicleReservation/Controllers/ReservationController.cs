﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VehicleReservation.ViewModels;
using VehicleReservation.Repository;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace VehicleReservation.Controllers
{
    public class ReservationController : Controller
    {
        VehicleReservationEntities db = new VehicleReservationEntities();

        //LIST
        public ActionResult List()
        {
            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            var repo = new ManagementRepository();
            //var model = repo.GetReserveHistory();
            ReservationHistoryVM RHVM = new ReservationHistoryVM();
            RHVM.Reservations = new List<ReservationViewModel>();

            RHVM.Reservations = (from data in db.ReservedVehicleDetails
                                 join b in db.Vehicles
                                 on data.VehicleID equals b.VehicleID into v
                                 from vh in v.DefaultIfEmpty()
                                 join c in db.Drivers 
                                 on data.DriverID equals c.DriverID into d
                                 from dr in d.DefaultIfEmpty()
                                 select new ReservationViewModel()
                                 {
                                     Id = data.Id,
                                     //BorrowerFullName = data.BorrowerLastName + ", " + data.BorrowerFirstName,
                                     //BorrowerLastName = data.BorrowerLastName,
                                     //BorrowerFirstName = data.BorrowerFirstName,
                                     BorrowerName = data.BorrowerName,
                                     VehicleID = data.VehicleID,
                                     DriverID = data.DriverID,
                                     Location = data.Location,
                                     DateTimeStart = data.DateTimeStart,
                                     DateTimeEnd = data.DateTimeEnd,
                                     VehicleMake = vh.VehicleMake,
                                     PlateNumber = vh.PlateNumber,
                                     IsActive = data.IsActive,
                                     IsComplete = data.IsComplete,
                                     DriverName = dr.Name,
                                     ReservedDate = data.DateTimeStart.ToString() + " - " + data.DateTimeEnd.ToString()
                                     
                                 }).ToList();

            return View(RHVM);
        }

        //DETAILS
        public ActionResult Details(int? id)
        {

            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            ReservedVehicleDetail RVD = db.ReservedVehicleDetails.Find(id);
            var RVM = new ReservationViewModel();
            var existingRes = (from a in db.ReservedVehicleDetails
                               join b in db.Drivers
                               on a.DriverID equals b.DriverID into dr
                               from d in dr.DefaultIfEmpty()
                               join c in db.Vehicles
                               on a.VehicleID equals c.VehicleID into vh
                               from v in vh.DefaultIfEmpty()
                               where a.Id == id
                               select new
                               {
                                   a.Id,
                                   a.BorrowerName,
                                   a.VehicleID,
                                   a.DriverID,
                                   a.OdometerStart,
                                   a.OdometerEnd,
                                   a.Liters,
                                   a.AmountPaid,
                                   a.ORNumber,
                                   a.Location,
                                   a.Reason,
                                   a.ProjectID,
                                   a.DateCreated,
                                   a.DateTimeStart,
                                   a.DateTimeEnd,
                                   //a.UserID,
                                   a.BusinessUnit,
                                   a.IsActive,
                                   a.ActualDateTimeStart,
                                   a.ActualDateTimeEnd,
                                   a.LocationParked,
                                   //b.DriverLastName,
                                   //b.DriverFirstName,
                                   d.Name,
                                   v.VehicleMake,
                                   v.VehicleType,
                                   v.PlateNumber,
                                   a.VehicleTypeID,
                                   a.IsComplete,

                               }).FirstOrDefault();
            RVM.Id = existingRes.Id;
            RVM.BorrowerName = existingRes.BorrowerName;
            RVM.VehicleID = existingRes.VehicleID;
            RVM.VehicleMake = existingRes.VehicleMake;
            RVM.VehicleType = existingRes.VehicleType;
            RVM.PlateNumber = existingRes.PlateNumber;
            RVM.DriverID = existingRes.DriverID;
            RVM.DriverName = existingRes.Name;
            RVM.OdometerStart = existingRes.OdometerStart;
            RVM.OdometerEnd = existingRes.OdometerEnd;
            RVM.Liters = existingRes.Liters;
            RVM.AmountPaid = existingRes.AmountPaid;
            RVM.ORNumber = existingRes.ORNumber;
            RVM.Location = existingRes.Location;
            RVM.LocationParked = existingRes.LocationParked;
            RVM.Reason = existingRes.Reason;
            RVM.ProjectID = existingRes.ProjectID;
            //RVM.UserID = existingRes.UserID;
            RVM.DateTimeStart = existingRes.DateTimeStart;
            RVM.DateTimeEnd = existingRes.DateTimeEnd;
            RVM.ActualDateTimeStart = existingRes.ActualDateTimeStart;
            RVM.ActualDateTimeEnd = existingRes.ActualDateTimeEnd;
            RVM.VehicleMakeLicense = existingRes.VehicleMake + " - " + existingRes.PlateNumber;
            RVM.BusinessUnit = existingRes.BusinessUnit;
            RVM.VehicleTypeID = existingRes.VehicleTypeID;
            RVM.IsActive = existingRes.IsActive;
            RVM.IsComplete = existingRes.IsComplete;

            if (RVM.ORNumber == null)
            {
                RVM.ORNumber = "N/A";
            }
            if (RVM.Liters == null)
            {
                RVM.Liters = "N/A";
            }
            if (RVM.AmountPaid == null)
            {
                ViewBag.AmountPaid = "N/A";

            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(RVM);
        }

        //RESERVE
       
        //public ActionResult ReserveInitial()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult ReserveInitial(ReservationViewModel RVM)
        //{
        //    var repo = new ManagementRepository();
        //    var resini = new ReservedVehicleDetail
        //    {
        //        Id = RVM.Id,
        //        BorrowerLastName = RVM.BorrowerLastName,
        //        BorrowerFirstName = RVM.BorrowerFirstName,
        //        VehicleID = RVM.VehicleID,
        //        Reason = RVM.Reason,
        //        ProjectID = RVM.ProjectID,
        //        Location = RVM.Location,
        //        DateTimeStart = RVM.DateTimeStart,
        //        DateCreated = DateTime.Now,
        //        UserID = RVM.UserID,
        //        DriverID = RVM.DriverID,
        //        IsActive = false

        //    };



        //    repo.ReserveInitial(resini);
        //    //TempData["ID"] = resini.Id;
        //    //TempData.Keep();
        //    return RedirectToAction("ChooseVehicle", "Reservation", new { id = resini.Id });
        //}

        public async Task<ActionResult> ReserveInitial()
        {
            //ViewBag.VehicleID2 = db.Vehicles.Where(item => item.IsReserved == false).Select(a => new SelectListItem
            //{
            //    Value = a.VehicleID.ToString() + "_" + a.DriverID.ToString(),
            //    Text = a.VehicleMake + " - " + a.PlateNumber,
            //});


            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }
            ReservationViewModel RVM = new ReservationViewModel();

            string token = Convert.ToString(Request.Cookies["token"].Value.ToString());
            int? rb = Convert.ToInt32(Request.Cookies["UserID"].Value.ToString());
            RVM.RequestedBy = rb;



            if (db.Drivers == null)
            {
                ViewBag.Message = "There are no drivers registered.";
                return RedirectToAction("List", "Reservation");
            }

            if (db.Vehicles == null)
            {
                ViewBag.Message = "There are no vehicles registered.";
                return RedirectToAction("List", "Reservation");

            }




            DataService DS = new DataService();
            RVM.Users = await DS.GetUsers(token);

        
            ViewBag.VehicleID2 = db.Vehicles.Select(a => new SelectListItem
            {
                Value = a.VehicleID.ToString() + "_" + a.DriverID.ToString(),
                Text = a.VehicleMake + " - " + a.PlateNumber,
            });


            ViewBag.DriverID = db.Drivers.Select(a => new SelectListItem
            {
                Value = a.DriverID.ToString(),
                Text = a.Name
            });



            return View(RVM);
        }

        [HttpPost]
        public ActionResult ReserveInitial(ReservationViewModel RVM)
        {
            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            int? rb = Convert.ToInt32(Request.Cookies["UserID"].Value.ToString());

            var repo = new ManagementRepository();
            var resini = new ReservedVehicleDetail
            {
                Id = RVM.Id,
                //BorrowerLastName = RVM.BorrowerLastName,
                //BorrowerFirstName = RVM.BorrowerFirstName,
                BorrowerName = RVM.BorrowerName,
                BusinessUnit = RVM.BusinessUnit,
                BusinessUnitID = RVM.BusinessUnitID,
                VehicleID = RVM.VehicleID,
                Reason = RVM.Reason,
                ProjectID = RVM.ProjectID,
                Location = RVM.Location,
                DateTimeStart = RVM.DateTimeStart,
                DateTimeEnd = RVM.DateTimeEnd,
                DateCreated = DateTime.Now,
                UserID = RVM.UserID,
                DriverID = RVM.DriverID,
                IsActive = false,
                IsComplete = false,
                RequestedBy = RVM.RequestedBy,
                NumberofPassengers = RVM.NumberofPassenger,
                VehicleTypeID = RVM.VehicleTypeID
            };

            repo.ReserveInitial(resini);

            if(resini.VehicleID==null && resini.DriverID == null)
            {
                goto ExitHere;
            }

            var changeVehicle = (from data in db.Vehicles
                                 where data.VehicleID == resini.VehicleID
                                 select data).FirstOrDefault();
            changeVehicle.IsReserved = true;

            repo.UpdateVehicle(changeVehicle);

            var existingVehicle = (from a in db.Vehicles
                                   where a.VehicleID == resini.VehicleID
                                   select new
                                   {
                                       a.VehicleMake,
                                       a.VehicleType,
                                       a.PlateNumber
                                   }).SingleOrDefault();

            var existingDriver = (from b in db.Drivers
                                  where b.DriverID == resini.DriverID
                                  select new
                                  {
                                      //b.DriverLastName,
                                      //b.DriverFirstName
                                      b.Name
                                  }).SingleOrDefault();

            var newDriverVehicle = new DriverVehicle();
            {
                newDriverVehicle.VehicleID = resini.VehicleID;
                newDriverVehicle.DriverID = resini.DriverID;
                newDriverVehicle.ReservationID = resini.Id;
                newDriverVehicle.VehicleMake = existingVehicle.VehicleMake;
                newDriverVehicle.VehicleType = existingVehicle.VehicleType;
                newDriverVehicle.PlateNumber = existingVehicle.PlateNumber;
                //newDriverVehicle.DriverLastName = existingDriver.DriverLastName;
                //newDriverVehicle.DriverFirstName = existingDriver.DriverFirstName;
                newDriverVehicle.Name = existingDriver.Name;
                newDriverVehicle.StartDateTime = resini.DateTimeStart;
                newDriverVehicle.EndDateTime = resini.DateTimeEnd;
                newDriverVehicle.IsComplete = false;
                newDriverVehicle.DateCreated = DateTime.Now;

            }

            repo.AddDriverVehicle(newDriverVehicle);

            ExitHere:
            return RedirectToAction("List");

            
            //TempData["ID"] = resini.Id;
            //TempData.Keep();

           

        }

        [HttpPost]
        public ActionResult reservation_details(int? id)
        {
            var data = (from a in db.ReservedVehicleDetails
                               join b in db.Drivers
                               on a.DriverID equals b.DriverID into dr
                               from d in dr.DefaultIfEmpty()
                               join c in db.Vehicles
                               on a.VehicleID equals c.VehicleID into vh
                               from v in vh.DefaultIfEmpty()
                               where a.Id == id
                               select new
                               {
                                   a.Id,
                                   a.BorrowerName,
                                   a.VehicleID,
                                   a.DriverID,
                                   a.OdometerStart,
                                   a.OdometerEnd,
                                   a.Liters,
                                   a.AmountPaid,
                                   a.ORNumber,
                                   a.Location,
                                   a.Reason,
                                   a.ProjectID,
                                   a.DateCreated,
                                   a.DateTimeStart,
                                   a.DateTimeEnd,
                                   //a.UserID,
                                   a.BusinessUnit,
                                   a.IsActive,
                                   a.ActualDateTimeStart,
                                   a.ActualDateTimeEnd,
                                   a.LocationParked,
                                   //b.DriverLastName,
                                   //b.DriverFirstName,
                                   d.Name,
                                   v.VehicleMake,
                                   v.VehicleType,
                                   v.PlateNumber,
                                   a.VehicleTypeID,
                                   a.IsComplete,

                               }).SingleOrDefault();

            return Json(data, JsonRequestBehavior.AllowGet);
        }



        //RESERVE - CHOOSE VEHICLE
        public ActionResult ChooseVehicle(int? id)
        {


            // int id = (int)TempData["ID"];

            ReservedVehicleDetail resDet = db.ReservedVehicleDetails.Find(id);
            var RVM = new ReservationViewModel();
            ViewBag.VehicleID = db.Vehicles.Select(a => new SelectListItem
            {
                Value = a.VehicleID.ToString(),
                Text = a.VehicleMake + " - " + a.PlateNumber
            });
            //   ViewBag.VehicleID = new SelectList(db.Vehicles, "VehicleID", "VehicleMake");

            RVM.Id = resDet.Id;
            RVM.BorrowerLastName = resDet.BorrowerLastName;
            RVM.BorrowerFirstName = resDet.BorrowerFirstName;
            RVM.Reason = resDet.Reason;
            RVM.ProjectID = resDet.ProjectID;
            RVM.Location = resDet.Location;
            RVM.DateTimeStart = resDet.DateTimeStart;
            RVM.DateCreated = resDet.DateCreated;
            RVM.UserID = resDet.UserID;
            RVM.IsActive = resDet.IsActive;
            RVM.IsComplete = resDet.IsComplete;
            //RVM.DriverID = resIni.DriverID;


            TempData.Keep();
            return View(RVM);
        }

        [HttpPost]
        public ActionResult ChooseVehicle(ReservationViewModel RVM)
        {
            var repo = new ManagementRepository();
            var resDet = new ReservedVehicleDetail();
            //        int id = (int)TempData["ID"];


            using (var db = new VehicleReservationEntities())
            {
                var existingReservation = (from data in db.ReservedVehicleDetails
                                           where data.Id == RVM.Id
                                           select data).FirstOrDefault();
                existingReservation.Id = RVM.Id;
                existingReservation.BorrowerLastName = RVM.BorrowerLastName;
                existingReservation.BorrowerFirstName = RVM.BorrowerFirstName;
                existingReservation.VehicleID = RVM.VehicleID;
                existingReservation.Reason = RVM.Reason;
                existingReservation.ProjectID = RVM.ProjectID;
                existingReservation.Location = RVM.Location;
                existingReservation.DateTimeStart = RVM.DateTimeStart;
                existingReservation.DateCreated = RVM.DateCreated;
                existingReservation.UserID = RVM.UserID;
                existingReservation.IsActive = false;
                //existingVehicle.DriverID = RVM.DriverID;

                repo.chooseVehicle(existingReservation);
            }



            return RedirectToAction("DriverCheck", "Reservation", new { id = RVM.Id });
        }

        //RESERVE - DRIVER CHECK
        public ActionResult DriverCheck(int? id)
        {
            ReservedVehicleDetail resDet = db.ReservedVehicleDetails.Find(id);
            var RVM = new ReservationViewModel();

            /*    var pickdriver = from a in db.ReservedVehicleInitials
                                 join b in db.Vehicles
                                 on a.DriverID equals b.DriverID
                                 where a.Id == id && b.VehicleID > 0
                                 select new ReservationViewModel()
                                 {

                                 };
            */
            var pickdriver = (from a in db.ReservedVehicleDetails
                              join b in db.Vehicles
                              on a.VehicleID equals b.VehicleID
                              where a.Id == id
                              select new
                              {
                                  a.Id,
                                  a.BorrowerLastName,
                                  a.BorrowerFirstName,
                                  a.UserID,
                                  a.VehicleID,
                                  a.Reason,
                                  a.ProjectID,
                                  a.Location,
                                  a.DateTimeStart,
                                  a.DateCreated,
                                  b.DriverID,
                                  a.IsActive
                              }).FirstOrDefault();
            RVM.Id = pickdriver.Id;
            RVM.DriverID = pickdriver.DriverID;
            RVM.VehicleID = pickdriver.VehicleID;
            RVM.BorrowerLastName = pickdriver.BorrowerLastName;
            RVM.BorrowerFirstName = pickdriver.BorrowerFirstName;
            RVM.UserID = pickdriver.UserID;
            RVM.Reason = pickdriver.Reason;
            RVM.ProjectID = pickdriver.ProjectID;
            RVM.Location = pickdriver.Location;
            RVM.DateTimeStart = pickdriver.DateTimeStart;
            RVM.DateCreated = pickdriver.DateCreated;
            RVM.IsActive = pickdriver.IsActive;

            if (RVM.DriverID == 0)
                return RedirectToAction("ChooseDriver", "Reservation", new { id = id });

            else
                ViewBag.DriverID = db.Drivers.Where(a => a.DriverID == RVM.DriverID).Select(a => new SelectListItem
                {
                    Value = a.DriverID.ToString(),
                    Text = a.DriverLastName + ", " + a.DriverFirstName
                });
            return View(RVM);

        }

        public Boolean checkDriver(int? id)
        {
            ReservedVehicleDetail resDet = db.ReservedVehicleDetails.Find(id);
            var RVM = new ReservationViewModel();

            /*    var pickdriver = from a in db.ReservedVehicleInitials
                                 join b in db.Vehicles
                                 on a.DriverID equals b.DriverID
                                 where a.Id == id && b.VehicleID > 0
                                 select new ReservationViewModel()
                                 {

                                 };
            */
            var pickdriver = (from a in db.ReservedVehicleDetails
                              join b in db.Vehicles
                              on a.VehicleID equals b.VehicleID
                              where a.Id == id
                              select new
                              {
                                  a.Id,
                                  a.BorrowerLastName,
                                  a.BorrowerFirstName,
                                  a.UserID,
                                  a.VehicleID,
                                  a.Reason,
                                  a.ProjectID,
                                  a.Location,
                                  a.DateTimeStart,
                                  a.DateCreated,
                                  b.DriverID,
                                  a.IsActive,
                                  a.IsComplete

                              }).FirstOrDefault();
            RVM.Id = pickdriver.Id;
            RVM.DriverID = pickdriver.DriverID;
            RVM.VehicleID = pickdriver.VehicleID;
            RVM.BorrowerLastName = pickdriver.BorrowerLastName;
            RVM.BorrowerFirstName = pickdriver.BorrowerFirstName;
            RVM.UserID = pickdriver.UserID;
            RVM.Reason = pickdriver.Reason;
            RVM.ProjectID = pickdriver.ProjectID;
            RVM.Location = pickdriver.Location;
            RVM.DateTimeStart = pickdriver.DateTimeStart;
            RVM.DateCreated = pickdriver.DateCreated;
            RVM.IsActive = pickdriver.IsActive;


            if (RVM.DriverID == 0)
                return true;

            else
                ViewBag.DriverID = db.Drivers.Where(a => a.DriverID == RVM.DriverID).Select(a => new SelectListItem
                {
                    Value = a.DriverID.ToString(),
                    Text = a.DriverLastName + ", " + a.DriverFirstName
                });
            return false;
        }

        [HttpPost]
        public ActionResult DriverCheck(ReservationViewModel RVM)
        {
            var repo = new ManagementRepository();
            using (var db = new VehicleReservationEntities())
            {
                var existingReservation = (from data in db.ReservedVehicleDetails
                                           where data.Id == RVM.Id
                                           select data).FirstOrDefault();
                existingReservation.Id = RVM.Id;
                existingReservation.BorrowerLastName = RVM.BorrowerLastName;
                existingReservation.BorrowerFirstName = RVM.BorrowerFirstName;
                existingReservation.VehicleID = RVM.VehicleID;
                existingReservation.Reason = RVM.Reason;
                existingReservation.ProjectID = RVM.ProjectID;
                existingReservation.Location = RVM.Location;
                existingReservation.DateTimeStart = RVM.DateTimeStart;
                existingReservation.DateCreated = RVM.DateCreated;
                existingReservation.UserID = RVM.UserID;
                existingReservation.DriverID = RVM.DriverID;
                existingReservation.IsActive = false;

                repo.chooseVehicle(existingReservation);
            }

            return RedirectToAction("List");
        }

        //RESERVE - CHOOSE DRIVER
        public ActionResult ChooseDriver(int? id)
        {
            ReservedVehicleDetail resDet = db.ReservedVehicleDetails.Find(id);
            var RVM = new ReservationViewModel();
            ViewBag.DriverID = db.Drivers.Select(a => new SelectListItem
            {
                Value = a.DriverID.ToString(),
                Text = a.DriverLastName + ", " + a.DriverFirstName
            });
            //ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "DriverLastName");


            RVM.Id = resDet.Id;
            RVM.BorrowerLastName = resDet.BorrowerLastName;
            RVM.BorrowerFirstName = resDet.BorrowerFirstName;
            RVM.Reason = resDet.Reason;
            RVM.ProjectID = resDet.ProjectID;
            RVM.Location = resDet.Location;
            RVM.DateTimeStart = resDet.DateTimeStart;
            RVM.DateCreated = resDet.DateCreated;
            RVM.UserID = resDet.UserID;
            RVM.IsActive = false;
            //  RVM.VehicleID = resIni.VehicleID;

            //TempData.Keep();
            return View(RVM);
        }

        [HttpPost]
        public ActionResult ChooseDriver(ReservationViewModel RVM)
        {
            var repo = new ManagementRepository();
            using (var db = new VehicleReservationEntities())
            {
                var existingReservation = (from data in db.ReservedVehicleDetails
                                           where data.Id == RVM.Id
                                           select data).FirstOrDefault();
                existingReservation.Id = RVM.Id;
                existingReservation.BorrowerLastName = RVM.BorrowerLastName;
                existingReservation.BorrowerFirstName = RVM.BorrowerFirstName;
                // existingReservation.VehicleID = RVM.VehicleID;
                existingReservation.Reason = RVM.Reason;
                existingReservation.ProjectID = RVM.ProjectID;
                existingReservation.Location = RVM.Location;
                existingReservation.DateTimeStart = RVM.DateTimeStart;
                existingReservation.DateCreated = RVM.DateCreated;
                existingReservation.UserID = RVM.UserID;
                existingReservation.DriverID = RVM.DriverID;
                existingReservation.IsActive = false;

                repo.chooseDriver(existingReservation);
            }
            return RedirectToAction("List");
        }


        ////RESERVE START
        [HttpGet]
        public ActionResult ViewReserveStart(int? id)
        {
            ReservedVehicleDetail resDet = db.ReservedVehicleDetails.Find(id);
            var RVM = new ReservationViewModel();
            var repo = new ManagementRepository();


            RVM.Id = resDet.Id;
            RVM.BorrowerLastName = resDet.BorrowerLastName;
            RVM.BorrowerFirstName = resDet.BorrowerFirstName;
            RVM.VehicleID = resDet.VehicleID;
            RVM.Reason = resDet.Reason;
            RVM.ProjectID = resDet.ProjectID;
            RVM.Location = resDet.Location;
            RVM.DateTimeStart = resDet.DateTimeStart;
            RVM.DateCreated = resDet.DateCreated;
            RVM.UserID = resDet.UserID;
            RVM.DriverID = resDet.DriverID;
            RVM.IsActive = true;
            RVM.DateTimeEnd = resDet.DateTimeEnd;
            RVM.OdometerStart = resDet.OdometerStart;
            RVM.OdometerEnd = resDet.OdometerEnd;
            RVM.Liters = resDet.Liters;
            RVM.AmountPaid = resDet.AmountPaid;
            RVM.ORNumber = resDet.ORNumber;
            RVM.LocationParked = resDet.LocationParked;
            RVM.IsComplete = false;
            RVM.ActualDateTimeStart = resDet.ActualDateTimeStart;
            RVM.ActualDateTimeEnd = resDet.ActualDateTimeEnd;
            RVM.BusinessUnit = resDet.BusinessUnit;
            RVM.BusinessUnitID = resDet.BusinessUnitID;
            RVM.BorrowerName = resDet.BorrowerName;

            var checkCoding = (from data in db.Vehicles
                               where data.VehicleID == RVM.VehicleID
                               select data).FirstOrDefault();

            RVM.CodingDay = checkCoding.CodingDay;

            return PartialView("_ReserveStart", RVM);
        }

        [HttpPost]
        public ActionResult ReserveStart(ReservationViewModel RVM)
        {
            var repo = new ManagementRepository();

            var existingReservation = (from data in db.ReservedVehicleDetails
                                       where data.Id == RVM.Id
                                       select data).FirstOrDefault();

            existingReservation.Id = RVM.Id;
            //existingReservation.BorrowerLastName = RVM.BorrowerLastName;
            //existingReservation.BorrowerFirstName = RVM.BorrowerFirstName;
            existingReservation.VehicleID = RVM.VehicleID;
            existingReservation.Reason = RVM.Reason;
            existingReservation.ProjectID = RVM.ProjectID;
            existingReservation.Location = RVM.Location;
            existingReservation.DateTimeStart = RVM.DateTimeStart;
            existingReservation.DateCreated = RVM.DateCreated;
            existingReservation.UserID = RVM.UserID;
            existingReservation.DriverID = RVM.DriverID;
            existingReservation.IsActive = true;
            existingReservation.DateTimeEnd = RVM.DateTimeEnd;
            existingReservation.OdometerStart = RVM.OdometerStart;
            existingReservation.OdometerEnd = RVM.OdometerEnd;
            existingReservation.Liters = RVM.Liters;
            existingReservation.AmountPaid = RVM.AmountPaid;
            existingReservation.ORNumber = RVM.ORNumber;
            existingReservation.LocationParked = RVM.LocationParked;
            existingReservation.IsComplete = false;
            existingReservation.ActualDateTimeStart = RVM.ActualDateTimeStart;
            existingReservation.ActualDateTimeEnd = RVM.ActualDateTimeEnd;
            existingReservation.BusinessUnit = RVM.BusinessUnit;
            existingReservation.BusinessUnitID = RVM.BusinessUnitID;
            existingReservation.BorrowerName = RVM.BorrowerName;


            repo.reserveStart(existingReservation);

            var changeVehicle = (from data in db.Vehicles
                                 where data.VehicleID == existingReservation.VehicleID
                                 select data).FirstOrDefault();
            changeVehicle.IsActive = true;

            repo.UpdateVehicle(changeVehicle);

            //return Json(existingReservation, JsonRequestBehavior.AllowGet);
            return RedirectToAction("List");
        }

        //public ActionResult ReserveStart(int? id)
        //{
        //    ReservedVehicleDetail resDet = db.ReservedVehicleDetails.Find(id);
        //    var RVM = new ReservationViewModel();

        //    RVM.Id = resDet.Id;
        //    RVM.BorrowerLastName = resDet.BorrowerLastName;
        //    RVM.BorrowerFirstName = resDet.BorrowerFirstName;
        //    RVM.Reason = resDet.Reason;
        //    RVM.ProjectID = resDet.ProjectID;
        //    RVM.Location = resDet.Location;
        //    RVM.DateTimeStart = resDet.DateTimeStart;
        //    RVM.DateCreated = resDet.DateCreated;
        //    RVM.UserID = resDet.UserID;
        //    RVM.DriverID = resDet.DriverID;
        //    RVM.VehicleID = resDet.VehicleID;
        //    RVM.IsActive = resDet.IsActive;
        //    /*   RVM.TimeEnd = rInstall-Package Bootstrap.v3.Datetimepicker.CSSesDet.TimeEnd;
        //       RVM.OdometerStart = resDet.OdometerStart;
        //       RVM.OdometerEnd = resDet.OdometerEnd;
        //       RVM.Liters = resDet.Liters;
        //       RVM.AmountPaid = resDet.AmountPaid;
        //       RVM.ORNumber = resDet.ORNumber;
        //       RVM.LocationParked = resDet.LocationParked;
        //       */

        //    return View(RVM);
        //}

        //[HttpPost]
        //public ActionResult ReserveStart(ReservationViewModel RVM)
        //{
        //    var repo = new ManagementRepository();
        //    var resDet = new ReservedVehicleDetail();

        //    var existingReservation = (from data in db.ReservedVehicleDetails
        //                               where data.Id == RVM.Id
        //                               select data).FirstOrDefault();

        //    existingReservation.Id = RVM.Id;
        //    existingReservation.BorrowerLastName = RVM.BorrowerLastName;
        //    existingReservation.BorrowerFirstName = RVM.BorrowerFirstName;
        //    existingReservation.VehicleID = RVM.VehicleID;
        //    existingReservation.Reason = RVM.Reason;
        //    existingReservation.ProjectID = RVM.ProjectID;
        //    existingReservation.Location = RVM.Location;
        //    existingReservation.DateTimeStart = RVM.DateTimeStart;
        //    existingReservation.DateCreated = RVM.DateCreated;
        //    existingReservation.UserID = RVM.UserID;
        //    existingReservation.DriverID = RVM.DriverID;
        //    existingReservation.IsActive = true;
        //    existingReservation.DateTimeEnd = RVM.DateTimeEnd;
        //    existingReservation.OdometerStart = RVM.OdometerStart;
        //    existingReservation.OdometerEnd = RVM.OdometerEnd;
        //    existingReservation.Liters = RVM.Liters;
        //    existingReservation.AmountPaid = RVM.AmountPaid;
        //    existingReservation.ORNumber = RVM.ORNumber;
        //    existingReservation.LocationParked = RVM.LocationParked;

        //    repo.reserveStart(existingReservation);


        //    return RedirectToAction("List");
        //}

        //RESERVE END
        [HttpGet]
        public ActionResult ViewReserveEnd(int? id)
        {
                ReservedVehicleDetail resDet = db.ReservedVehicleDetails.Find(id);
                var RVM = new ReservationViewModel();

                RVM.Id = resDet.Id;
                RVM.BorrowerLastName = resDet.BorrowerLastName;
                RVM.BorrowerFirstName = resDet.BorrowerFirstName;
                RVM.Reason = resDet.Reason;
                RVM.ProjectID = resDet.ProjectID;
                RVM.Location = resDet.Location;
                RVM.DateTimeStart = resDet.DateTimeStart;
                RVM.DateCreated = resDet.DateCreated;
                RVM.UserID = resDet.UserID;
                RVM.DriverID = resDet.DriverID;
                RVM.VehicleID = resDet.VehicleID;
                RVM.IsActive = resDet.IsActive;
                RVM.DateTimeEnd = resDet.DateTimeEnd;
                RVM.OdometerStart = resDet.OdometerStart;
                RVM.OdometerEnd = resDet.OdometerEnd;
                RVM.Liters = resDet.Liters;
                RVM.AmountPaid = resDet.AmountPaid;
                RVM.ORNumber = resDet.ORNumber;
                RVM.LocationParked = resDet.LocationParked;
                RVM.IsComplete = resDet.IsComplete;
                RVM.ActualDateTimeEnd = resDet.ActualDateTimeEnd;
                RVM.ActualDateTimeStart = resDet.ActualDateTimeStart;
                RVM.BusinessUnit = resDet.BusinessUnit;
                RVM.BusinessUnitID = resDet.BusinessUnitID;
                RVM.BorrowerName = resDet.BorrowerName;

            var checkCoding = (from data in db.Vehicles
                               where data.VehicleID == RVM.VehicleID
                               select data).FirstOrDefault();

            RVM.CodingDay = checkCoding.CodingDay;

            return PartialView("_ReserveEnd", RVM);
        }

        [HttpPost]
        public ActionResult ReserveEnd(ReservationViewModel RVM)
        {
            var repo = new ManagementRepository();
            var resDet = new ReservedVehicleDetail();

            var existingReservation = (from data in db.ReservedVehicleDetails
                                       where data.Id == RVM.Id
                                       select data).FirstOrDefault();

            existingReservation.Id = RVM.Id;
            existingReservation.BorrowerLastName = RVM.BorrowerLastName;
            existingReservation.BorrowerFirstName = RVM.BorrowerFirstName;
            existingReservation.VehicleID = RVM.VehicleID;
            existingReservation.Reason = RVM.Reason;
            existingReservation.ProjectID = RVM.ProjectID;
            existingReservation.Location = RVM.Location;
            existingReservation.DateTimeStart = RVM.DateTimeStart;
            existingReservation.DateTimeEnd = RVM.DateTimeEnd;
            existingReservation.DateCreated = RVM.DateCreated;
            existingReservation.UserID = RVM.UserID;
            existingReservation.DriverID = RVM.DriverID;
            existingReservation.IsActive = false;
            existingReservation.OdometerStart = RVM.OdometerStart;
            existingReservation.OdometerEnd = RVM.OdometerEnd;
            existingReservation.Liters = RVM.Liters;
            existingReservation.AmountPaid = RVM.AmountPaid;
            existingReservation.ORNumber = RVM.ORNumber;
            existingReservation.LocationParked = RVM.LocationParked;
            existingReservation.IsComplete = true;
            existingReservation.ActualDateTimeStart = RVM.ActualDateTimeStart;
            existingReservation.ActualDateTimeEnd = RVM.ActualDateTimeEnd;
            existingReservation.BusinessUnit = RVM.BusinessUnit;
            existingReservation.BusinessUnitID = RVM.BusinessUnitID;
            existingReservation.BorrowerName = RVM.BorrowerName;

            repo.reserveEnd(existingReservation);

            var changeVehicle = (from data in db.Vehicles
                                 where data.VehicleID == existingReservation.VehicleID
                                 select data).FirstOrDefault();
            changeVehicle.IsActive = false;
            changeVehicle.IsReserved = false;

            repo.UpdateVehicle(changeVehicle);

            var changeDriverVehicle = (from data in db.DriverVehicles
                                       where data.ReservationID == existingReservation.Id
                                 select data).FirstOrDefault();
            changeDriverVehicle.IsComplete = true;

            repo.UpdateDriverVehicle(changeDriverVehicle);

            return RedirectToAction("List");
        }

        //RESERVE START - TEST

            public ActionResult ViewTest()
        {
            return PartialView("_Test");
        }

        [HttpPost]
        public ActionResult Test()
        {
            return RedirectToAction("List");
        }

        //CALENDAR
        public ActionResult Calendar()
        {
            if (Request.Cookies["UserID"] == null || Request.Cookies["token"] == null)
            {
                return Redirect("https://www.portal.smsgt.com/webportal/index.php/login");
            }


            return View();
        }

        //EVENTS
        //public class MyEvent
        //{
        //    public String title;
        //    public String start;
        //    public int id;
        //    public String allDay;

        //}

        //public String Events(string start, string end)
        //{

        //    var myEvents = new List<MyEvent>();

        //    var myEvent = new MyEvent
        //    {
        //        title = "WALANG PASOK",
        //        start = "2018-07-29",
        //        id = 1,
        //        allDay = ""

        //    };

        //    myEvents.Add(myEvent);
        //    var json = JsonConvert.SerializeObject(myEvents);


        //    return json;
        //}
        //public class eventClass
        //{
        //    public int id;
        //    public string Title;
        //    public string Start;
        //    public string allDay;
        //    public string Description;

        //}

        public String Events(string start, string end)
        {


            EventList EL = new EventList();
            EL.Events = new List<EventViewModel>();

            EL.Events = (from a in db.ReservedVehicleDetails
                         join b in db.Drivers on a.DriverID equals b.DriverID
                         join c in db.Vehicles on a.VehicleID equals c.VehicleID
                         select new EventViewModel()
                         {
                             title = a.BorrowerName + "'s Reservation",
                             // a.TimeStart,
                             //start = a.DateTimeStart.ToString(),
                             start = a.DateTimeStart,
                             end = a.DateTimeEnd,
                             description = "Driver: " + b.Name,
                             description2 = "Vehicle: " + c.VehicleMake + " - " + c.PlateNumber,
                             description3 = "Time: " + a.DateTimeStart.ToString() + " - " + a.DateTimeEnd.ToString(),
                             isActive = a.IsActive,
                             isComplete = a.IsComplete                            
                             //b.DriverFirstName,
                             //  b.DriverLastName,
                             //c.VehicleMake,
                             //  c.PlateNumber
                         }).ToList();

            var json = JsonConvert.SerializeObject(EL.Events);
            return json;
            
        }

        public String checkVehicle(int VehicleID)
        {


            ReservedDatesList RDL = new ReservedDatesList();
            RDL.Dates = new List<ReservedDates>();

            RDL.Dates = (from a in db.DriverVehicles
                         where a.IsComplete == false && a.VehicleID == VehicleID
                         select new ReservedDates()
                         {
                            DateTimeStart = a.StartDateTime,
                            DateTimeEnd = a.EndDateTime,
                            PlateNumber = a.PlateNumber,
                            VehicleMake = a.VehicleMake

                         }).ToList();

            var json = JsonConvert.SerializeObject(RDL.Dates);
            return json;

        }

        public String checkDriver(int DriverID)
        {


            ReservedDatesList RDL = new ReservedDatesList();
            RDL.Dates = new List<ReservedDates>();

            RDL.Dates = (from a in db.DriverVehicles
                         where a.IsComplete == false && a.DriverID == DriverID
                         select new ReservedDates()
                         {
                             DateTimeStart = a.StartDateTime,
                             DateTimeEnd = a.EndDateTime,
                             Name = a.Name

                         }).ToList();

            var json = JsonConvert.SerializeObject(RDL.Dates);
            return json;

        }


        public string checkCodingDay (int VehicleID)
        {
            CarList CL = new CarList();
            CL.Cars = new List<CarViewModel>();

            CL.Cars = (from a in db.Vehicles
                       where a.VehicleID == VehicleID
                       select new CarViewModel()
                       {
                           CodingDay = a.CodingDay

                       }).ToList();
            var json = JsonConvert.SerializeObject(CL.Cars);
            return json;
        }
        
        public ActionResult LoginTest()
        {

            
            return View();
        }   

    }
}