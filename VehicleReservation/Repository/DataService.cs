﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using VehicleReservation.ViewModels;

namespace VehicleReservation.Repository
{
    public class DataService
    {

        //private HttpClient client = new HttpClient();

        //private async Task RunAsync()
        //{
        //    client.BaseAddress = new Uri("https://www.portal.smsgt.com/onesapi/api/attendance/Getusers");
        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(
        //        new MediaTypeWithQualityHeaderValue("application/json"));
        //}


        public async Task<List<Users>> GetUsers(string token)
        {
            try
            {
                HttpClient client = new HttpClient();

                client.BaseAddress = new Uri("https://www.portal.smsgt.com/onesapi/api/attendance/Getusers");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("token", token);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                    

                var UVM = new List<Users>();
                HttpResponseMessage response = await client.GetAsync("");

                if (response.IsSuccessStatusCode)
                {
                    UVM = await response.Content.ReadAsAsync<List<Users>>();
                }

                return UVM;
            }
            catch (Exception)
            {
                return new List<Users>();
            }

        }
    }
}