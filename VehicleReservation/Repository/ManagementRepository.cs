﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace VehicleReservation.Repository
{
    public class ManagementRepository
    {
        //DRIVER
        public void AddDriver(Driver newDriver)
        {
            using (var db = new VehicleReservationEntities())
            {

                db.Drivers.Add(newDriver);
                db.SaveChanges();
            }
        }

        public void UpdateDriver(Driver editDriver)
        {
            using (var db = new VehicleReservationEntities())
            {
                // var existingDriver = (from data in db.Drivers
                //                       where data.DriverID == editDriver.DriverID
                //                       select data).FirstOrDefault();

                db.Entry(editDriver).State = EntityState.Modified;
                db.SaveChanges();
            }

            /*            using (var db = new VehicleReservationEntities())
                       {

                           db.Entry(id).State = EntityState.Modified;
                           db.SaveChanges();
                       }
           */


        }

        public List<Driver> ListDriver()
        {
            using (var db = new VehicleReservationEntities())
            {
                var model = (from data in db.Drivers.ToList()
                             select new Driver()
                             {
                                 DriverID = data.DriverID,
                                 DriverLicense = data.DriverLicense,
                                 Name = data.Name,
                                 LicenseExpiry = data.LicenseExpiry,
                                 BusinessUnit = data.BusinessUnit,
                                 MobileNumber = data.MobileNumber

                             }).ToList();
                return model;
            }
        }

        public Driver GetDriverDetails(int DriverID)
        {
            using (var db = new VehicleReservationEntities())
            {
                var driver = (from data in db.Drivers
                              where data.DriverID == DriverID
                              select data).SingleOrDefault();

                // driver.DriverID 

                return driver;
            }

        }

        //VEHICLE
        public void AddVehicle(Vehicle newVehicle)
        {
            using (var db = new VehicleReservationEntities())
            {

                db.Vehicles.Add(newVehicle);
                db.SaveChanges();
            }
        }

        public List<Vehicle> ListVehicle()
        {
            using (var db = new VehicleReservationEntities())
            {
                var model = (from data in db.Vehicles.ToList()
                             select new Vehicle()
                             {
                                 VehicleID = data.VehicleID,
                                 VehicleType = data.VehicleType,
                                 VehicleMake = data.VehicleMake,
                                 PlateNumber = data.PlateNumber,
                                 CodingDay = data.CodingDay,
                                 Location = data.Location,
                                 DriverID = data.DriverID,
                                 DateRegistered = data.DateRegistered
                             }).ToList();
                return model;

            }
        }


        public Vehicle GetVehicleDetails(int VehicleID)
        {
            using (var db = new VehicleReservationEntities())
            {
                var model = (from data in db.Vehicles
                             where data.VehicleID == VehicleID
                             select data).SingleOrDefault();
                return model;
            }
        }

        public void UpdateVehicle (Vehicle editVehicle)
        {
            using (var db = new VehicleReservationEntities())
            { 
                db.Entry(editVehicle).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //DRIVERVEHICLE
        public void AddDriverVehicle(DriverVehicle newDriverVehicle)
        {
            using (var db = new VehicleReservationEntities())
            {
                db.DriverVehicles.Add(newDriverVehicle);
                db.SaveChanges();
            }
        }

        public void UpdateDriverVehicle(DriverVehicle editDriverVehicle)
        {
            using (var db = new VehicleReservationEntities())
            {
                db.Entry(editDriverVehicle).State = EntityState.Modified;
                db.SaveChanges();
            }
        }


        //RESERVATION
        public void ReserveInitial(ReservedVehicleDetail newReservationInitial)
        {
            using (var db = new VehicleReservationEntities())
            {
                db.ReservedVehicleDetails.Add(newReservationInitial);
                db.SaveChanges();
            }
        }


        public List<ReservedVehicleDetail> GetReserveHistory()
        {
            using (var db = new VehicleReservationEntities())
            {
                    var model = (from data in db.ReservedVehicleDetails.ToList()
                             select new ReservedVehicleDetail()
                             {
                                 Id = data.Id,
                                 BorrowerLastName = data.BorrowerLastName,
                                 BorrowerFirstName = data.BorrowerFirstName,
                                 VehicleID = data.VehicleID,
                                 Reason = data.Reason,
                                 ProjectID = data.ProjectID,
                                 Location = data.Location,
                                 DateTimeStart = data.DateTimeStart,
                                 DateCreated = data.DateCreated,
                                 UserID = data.UserID,
                                 DriverID = data.DriverID,
                                 IsActive = data.IsActive

                             }).ToList();
                return model;

            }
        }


        public void chooseVehicle(ReservedVehicleDetail resDet)
        {
            using (var db = new VehicleReservationEntities())
            {
                db.Entry(resDet).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void chooseDriver(ReservedVehicleDetail resDet)
        {
            using (var db = new VehicleReservationEntities())
            {
                db.Entry(resDet).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void reserveStart(ReservedVehicleDetail resDet)
        {
            using (var db = new VehicleReservationEntities())
            {
                db.Entry(resDet).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void reserveEnd(ReservedVehicleDetail resDet)
        {
            using (var db = new VehicleReservationEntities())
            {
                db.Entry(resDet).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}