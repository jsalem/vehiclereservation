﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VehicleReservation.Startup))]
namespace VehicleReservation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
